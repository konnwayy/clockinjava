/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

/**
 *
 * @author Clint Shepherd
 */
public class currentTime {

    private Date instanceTime;

    public LocalTime time() {
        LocalTime instanceTime = ZonedDateTime.now().toLocalTime().truncatedTo(ChronoUnit.SECONDS);
        
        return instanceTime;
    }
    
    public String date(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
	LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        
        return (dtf.format(now));
    }
}
//System.currentTimeMillis();