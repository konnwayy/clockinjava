/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Clint Shepherd
 */
public class WriteClockIn implements Serializable{
    static ArrayList<TimeRecorder> timeRecord = new ArrayList<>();
    static ArrayList<EmployeeRecords> employeeRecords = new ArrayList<>();
    
    
    public void setTimeRecord(ArrayList timeRecord){
        this.timeRecord = timeRecord;
    }
    
    public void setEmployeeRecords(ArrayList employeeRecords){
        this.employeeRecords = employeeRecords;
    }
    
    
    public void writeEmployeeToFile() {
        ObjectOutputStream outStream;
        try {
            outStream = new ObjectOutputStream(
                    new FileOutputStream("employee.txt"));
            outStream.writeObject(timeRecord);
        } catch (IOException ex) {
            Logger.getLogger(WriteClockIn.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void writeDataToFile(){
        ObjectOutputStream outStream;
        try{
            outStream = new ObjectOutputStream(
                    new FileOutputStream("employeeRecords.txt"));
            outStream.writeObject(employeeRecords);
        }catch (IOException ex) {
            Logger.getLogger(WriteClockIn.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
