/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;

import java.io.Serializable;

/**
 *
 * @author Clint Shepherd
 */
public class TimeRecorder implements Serializable{
    private long In,breakOut,breakIn,out;
    private String name,password,condition;

    public TimeRecorder(String condition,String name,String password,long in, long breakOut, long breakIn,long out) {
        this.condition = condition;
        this.name = name;
        this.password = password;
        this.In = in;
        this.breakOut = breakOut;
        this.breakIn = breakIn;
        this.out = out;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setIn(long in) {
        this.In = in;
    }

    public void setBreakOut(long breakOut) {
        this.breakOut = breakOut;
    }

    public void setBreakIn(long breakIn) {
        this.breakIn = breakIn;
    }

    public void setOut(long Out) {
        this.out = Out;
    }

    public String getCondition() {
        return condition;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public long getIn() {
        return In;
    }

    public long getBreakOut() {
        return breakOut;
    }

    public long getBreakIn() {
        return breakIn;
    }

    public long getOut() {
        return out;
    }
    
    @Override
    public String toString(){
        return (In+":"+breakOut+":"+breakIn);
    }
}
