/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;

import java.io.Serializable;

/**
 *
 * @author Clint Shepherd
 */
public class EmployeeRecords implements Serializable{
    private String name,date,time;
    public EmployeeRecords(String name,String date,String time){
        this.name = name;
        this.date = date;
        this.time = time;
    }
    
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }
    public String getDate(){
        return date;
    }
    public void setDate(String date){
        this.date = date;
    }
    public String getTime(){
        return time;
    }
    public void setTime(String time){
        this.time = time;
    }
    
    @Override
    public String toString(){
        return (name + "    " + date + "    " + time);
    }
    
}
