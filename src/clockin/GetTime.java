/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
/**
 *
 * @author Clint Shepherd
 */
class GetTime {
    
    public LocalTime time(){
        LocalTime instanceTime = ZonedDateTime.now().toLocalTime().truncatedTo(ChronoUnit.SECONDS);
        return instanceTime;
    }
    
    public long getTimeInSeconds(){
        LocalDateTime date = LocalDateTime.now();
        long seconds = Duration.between(date.withSecond(0).withMinute(0).withHour(0),date).getSeconds();
        return seconds;
    }
    
    public String date(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        LocalDateTime now = LocalDateTime.now();
        System.out.println(dtf.format(now));
        return (dtf.format(now));
    }
}
