/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clockin;


import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author Clint Shepherd
 */
public class ReadClockIn implements Serializable {

    ArrayList<TimeRecorder> temp = new ArrayList<>();
    ArrayList<EmployeeRecords> records = new ArrayList<>();

    public ArrayList readEmployee() throws IOException, ClassNotFoundException {
        ObjectInputStream inStream = new ObjectInputStream(
                new FileInputStream("employee.txt"));
        temp = (ArrayList) inStream.readObject();
        return temp;
    }

    public ArrayList readRecords() throws IOException, ClassNotFoundException {
        ObjectInputStream inStream = new ObjectInputStream(
                new FileInputStream("employeeRecords.txt"));
        records = (ArrayList) inStream.readObject();
        return records;
    }

}
